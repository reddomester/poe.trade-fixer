var searchResultsDiv = $('.search-results-block');
var divItem = searchResultsDiv.find('div.item');
var tbodyItem = searchResultsDiv.find('tbody.item');
var mainRow = $('#search-results-0 > .row:first-child');
var mainTable = $('#search-results-0 > table#search-results-first');

console.log(divItem.length, mainTable.length, tbodyItem.length);

if (mainTable.length < 1) {
  divItem.each(function(){
    $(this).detach().appendTo(mainRow);
    if ($(this).find('.pricing-table .price').text().indexOf('Seller is online') > -1) {
      $(this).find('.pricing-table .description:last-child').append(' · <button class="whisper">Whisper</button>');
    }
    $(this).prepend('<button class="remove">&times;</button>');
  });
} else {
  tbodyItem.each(function(){
    $(this).detach().appendTo(mainTable);
  });
}

searchResultsDiv
  .find('.centered').remove().end()
  .find('#search-results-1').remove().end()
  .find('#search-results-2').remove();

searchResultsDiv.on('click', 'button.whisper', function(ev){
  var item = $(ev.target).closest(".item");
  var bo = item.data("buyout") ? " listed for " + item.data("buyout") : ". What are you asking for it?";
  var ign = $.trim(item.find('ul.pricing-table > .price').html().split("IGN: ")[1]);
  var title = $.trim(item.find('ul.pricing-table > .title').html().replace("<br>", " "));
  var message="@" + ign + " Hi, I would like to buy your " + title + bo;
  window.prompt("Copy message to clipboard by pressing Ctrl+C", message);
}).on('click', 'button.remove', function(ev){
  var item = $(ev.target).closest(".item");
  item.remove();
});